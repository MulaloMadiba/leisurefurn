﻿using System;
using System.Collections.Generic;
using System.Linq;
using Leisurefurn.DAL;
using Leisurefurn.Models;
using TinyCsvParser.Mapping;

namespace Leisurefurn
{
	internal static class Factory
	{
		internal static void InsertInvoiceData(LeisurefurnContext context, List<CsvMappingResult<InvoiceData>> invoiceData)
		{
			foreach (var invoice in invoiceData)
			{
				if (context.InvoiceHeaders.FirstOrDefault(i => i.InvoiceNumber == invoice.Result.InvoiceNumber) == null)
				{
					var invoiceHeader = (InvoiceHeader)invoice;
					context.InvoiceHeaders.Add(invoiceHeader);
				}

				var invoiceLines = (InvoiceLines)invoice;
				context.InvoiceLines.Add(invoiceLines);
				context.SaveChanges();
			}
		}

		internal static void GetInvoiceData(LeisurefurnContext context)
		{
			var invoiceList = context.InvoiceLines.Select(i => new {i.InvoiceNumber, i.Quantity})
				.GroupBy(x => new {x.InvoiceNumber})
				.Select(g => new { g.Key.InvoiceNumber, TotalQuantity = g.Sum(t => t.Quantity)});
			DisplayHeader();
			foreach (var invoice in invoiceList)
			{
				Console.WriteLine($"{invoice.InvoiceNumber}\t\t| {invoice.TotalQuantity}");
			}
		}

		private static void DisplayHeader()
		{
			Console.WriteLine($"{Constants.InvoiceNumberDisplay}\t| {Constants.QuantityDisplay}");
			Console.WriteLine(Constants.HeaderLine);
		}

		internal static void CheckBalance(LeisurefurnContext context, List<CsvMappingResult<InvoiceData>> invoiceData)
		{
			var csvTotal = GetCsvTotal(invoiceData);
			var dbTotal = GetDatabaseTotal(context);
			DisplayBalances(csvTotal, dbTotal);
		}

		private static void DisplayBalances(float csvTotal, float dbTotal)
		{
			Console.WriteLine();
			Console.WriteLine($"{Constants.CsvBalance} {csvTotal}");
			Console.WriteLine($"{Constants.DatabaseBalance} {dbTotal}");
			Console.WriteLine(csvTotal.Equals(dbTotal) ? Constants.BalanceMatch : Constants.BalancDontMatch);
		}

		private static float GetDatabaseTotal(LeisurefurnContext context)
		{
			var dbTotal = context.InvoiceLines.Select(i => new {i.Quantity, i.UnitSellingPriceExVAT})
				.Select(g => new {TotalQuantity = g.Quantity * g.UnitSellingPriceExVAT}).Sum(t => t.TotalQuantity);
			return dbTotal;
		}

		private static float GetCsvTotal(IEnumerable<CsvMappingResult<InvoiceData>> invoiceData)
		{
			var csvTotal = invoiceData.Sum(x => x.Result.Quantity * x.Result.UnitSellingPriceExVAT);
			return csvTotal;
		}
	}
}
