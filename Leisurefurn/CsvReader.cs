﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Leisurefurn.Mappings;
using Leisurefurn.Models;
using TinyCsvParser;
using TinyCsvParser.Mapping;

namespace Leisurefurn
{
	internal static class CsvReader
	{
		internal static List<CsvMappingResult<InvoiceData>> GetInvoiceData()
		{
			var csvParserOptions = new CsvParserOptions(true, Config.GetSettings().Seperator);
			var csvMapper = new CsvInvoiceHeaderMapping();
			var csvParser = new CsvParser<InvoiceData>(csvParserOptions, csvMapper);
			var result = GetCsvMappingResults(csvParser);
			return result;
		}

		private static List<CsvMappingResult<T>> GetCsvMappingResults<T>(CsvParser<T> csvParser)
		{
			var result = csvParser.ReadFromFile(Config.GetSettings().CsvPath, Encoding.ASCII).ToList();
			return result;
		}
	}
}
