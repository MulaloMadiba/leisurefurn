﻿using System;
using Leisurefurn.DAL;

namespace Leisurefurn
{
	internal class Program
	{
		private static void Main()
		{
			try
			{
				var context = new LeisurefurnContext();
				var invoiceData = CsvReader.GetInvoiceData();
				Factory.InsertInvoiceData(context, invoiceData);
				Factory.GetInvoiceData(context);
				Factory.CheckBalance(context, invoiceData);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}


	}
}
