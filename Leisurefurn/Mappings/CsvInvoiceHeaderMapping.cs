﻿using Leisurefurn.Models;
using TinyCsvParser.Mapping;
using TinyCsvParser.TypeConverter;

namespace Leisurefurn.Mappings
{
	public class CsvInvoiceHeaderMapping : CsvMapping<InvoiceData>
	{
		public CsvInvoiceHeaderMapping()
		{
			MapProperty(0, x => x.InvoiceNumber);
			MapProperty(1, x => x.InvoiceDate, new DateTimeConverter(Constants.DateFormat));
			MapProperty(2, x => x.Address);
			MapProperty(3, x => x.InvoiceTotal);
			MapProperty(4, x => x.Description);
			MapProperty(5, x => x.Quantity);
			MapProperty(6, x => x.UnitSellingPriceExVAT);

		}
	}
}
