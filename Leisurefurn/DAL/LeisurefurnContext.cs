﻿using Leisurefurn.Models;
using Microsoft.EntityFrameworkCore;

namespace Leisurefurn.DAL
{
	public class LeisurefurnContext  : DbContext
	{
		public DbSet<InvoiceHeader> InvoiceHeaders { get; set; }
		public DbSet<InvoiceLines> InvoiceLines { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{ 
			optionsBuilder.UseSqlServer(Config.GetSettings().ConnectionString);
		}
	}
}
