﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Leisurefurn.Migrations
{
    public partial class CreateLeisurefurnDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "InvoiceHeaders",
                table => new
                {
                    InvoiceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InvoiceNumber = table.Column<string>(maxLength: 50, nullable: false),
                    InvoiceDate = table.Column<DateTime>("Date", nullable: false),
                    Address = table.Column<string>(maxLength: 50, nullable: true),
                    InvoiceTotal = table.Column<double>("float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoiceHeaders", x => x.InvoiceId);
                });

            migrationBuilder.CreateTable(
                "InvoiceLines",
                table => new
                {
                    LineId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InvoiceNumber = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 100, nullable: true),
                    Quantity = table.Column<double>("float", nullable: false),
                    UnitSellingPriceExVAT = table.Column<double>("float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoiceLines", x => x.LineId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "InvoiceHeaders");

            migrationBuilder.DropTable(
                "InvoiceLines");
        }
    }
}
