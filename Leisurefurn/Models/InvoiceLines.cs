﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TinyCsvParser.Mapping;

namespace Leisurefurn.Models
{
	public class InvoiceLines
	{
		[Key]
		public int LineId { get; set; }
		[Required]
		[StringLength(50)]
		public string InvoiceNumber { get; set; }
		[StringLength(100)]
		public string Description { get; set; }
		[Column(TypeName = "float")]
		public float Quantity{ get; set; }
		[Column(TypeName = "float")]
		public float UnitSellingPriceExVAT { get; set; }

		public static explicit operator InvoiceLines(CsvMappingResult<InvoiceData> invoiceData)
		{
			return new InvoiceLines()
			{
				InvoiceNumber = invoiceData.Result.InvoiceNumber,
				Description = invoiceData.Result.Description,
				Quantity = invoiceData.Result.Quantity,
				UnitSellingPriceExVAT = invoiceData.Result.UnitSellingPriceExVAT
			};
		}
	}
}
