﻿using System;

namespace Leisurefurn.Models
{
	public class InvoiceData
	{
		public string InvoiceNumber { get; set; }
		public DateTime InvoiceDate { get; set; }
		public string Address { get; set; }
		public float InvoiceTotal { get; set; }
		public string Description { get; set; }
		public float Quantity{ get; set; }
		public float UnitSellingPriceExVAT { get; set; }
	}
}
