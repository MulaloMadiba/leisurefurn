﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TinyCsvParser.Mapping;

namespace Leisurefurn.Models
{
	public class InvoiceHeader
	{
		[Key]
		public int InvoiceId { get; set; }
		[Required]
		[StringLength(50)]
		public string InvoiceNumber { get; set; }
		[Column(TypeName = "Date")]
		public DateTime InvoiceDate { get; set; }
		[StringLength(50)]
		public string Address { get; set; }
		[Column(TypeName = "float")]
		public float InvoiceTotal { get; set; }

		public static explicit operator InvoiceHeader(CsvMappingResult<InvoiceData> invoiceData)
		{
			return new InvoiceHeader()
			{
				InvoiceNumber = invoiceData.Result.InvoiceNumber,
				Address = invoiceData.Result.Address,
				InvoiceDate = invoiceData.Result.InvoiceDate,
				InvoiceTotal = invoiceData.Result.InvoiceTotal
			};
		}

	}
}
