﻿namespace Leisurefurn
{
	internal class Constants
	{
		internal const string SettingsFileName = "appsettings.json";
		internal const string InvoiceNumberDisplay = "Invoice Number ";
		internal const string QuantityDisplay = "Total Quantity";
		internal const string DateFormat = "dd/MM/yyyy hh:mm";
		internal const string HeaderLine = "---------------------------------------";
		internal const string CsvBalance = "CSV file Balance:";
		internal const string DatabaseBalance = "Database Balance:";
		internal const string BalanceMatch = "The balances match.";
		internal const string BalancDontMatch = "The balances do not match.";
	}
}
