﻿namespace Leisurefurn
{
	public class Settings
	{
		public string ConnectionString { get; set; }

		public string CsvPath { get; set; }

		public char Seperator { get; set; }
	}
}
