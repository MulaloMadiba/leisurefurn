﻿using System.IO;
using Microsoft.Extensions.Configuration;

namespace Leisurefurn
{
	public class Config
	{
		public static Settings GetSettings()
		{
			var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(Constants.SettingsFileName, true, true);
			var configuration = builder.Build();
			var settings = new Settings();
			configuration.GetSection(nameof(Settings)).Bind(settings);

			return settings;
		}
	}
}
